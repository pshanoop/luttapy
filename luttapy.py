#!/usr/bin/python3 -u

import logging
import os
import pprint
import time
from telethon import TelegramClient, events

# import click
# from telethon.tl.types import Message, DocumentAttributeFilename

LOG_LEVEL = os.environ['LUTTAPY_LOG']
API_ID_HASH = os.environ['LUTTAPY_API'].split(':')
DOWNLOAD_PATH = os.environ['DOWNLOAD_PATH']
AUTH_USERS = os.environ['LUTTAPY_AUTH'].split(',')
print(AUTH_USERS)

logging.basicConfig(format='[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s',
                    level=LOG_LEVEL)

client = TelegramClient('luttapy', API_ID_HASH[0], API_ID_HASH[1])

@client.on(events.NewMessage)
async def my_event_handler(event):
    message = event.message

    if str(message.from_id) not in AUTH_USERS:
        print('unauthorized user %s' % message.from_id)
        return
    if 'ping' in event.raw_text:
        await event.reply('pong')

    if message.media is not None:
        tick = time.time()
        path = await message.download_media(file=DOWNLOAD_PATH)
        tock = time.time()
        download_time = (tock - tick) / 60
        reply = 'Download completed: {} mins File saved to: {}'.format(download_time, path)
        print(reply)
        await event.reply(reply)

    await client.send_read_acknowledge(entity=event.sender_id, message=message)


client.start()
print('Luttapy started...')
client.run_until_disconnected()
