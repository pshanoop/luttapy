DEPS := LICENSE luttapy.py luttapy.service

.PHONY: install

install: $(DEPS)

	mkdir -p /opt/luttapy
	cp ./LICENSE /opt/luttapy/
	cp ./luttapy.py /opt/luttapy/
	cp ./requirements.txt /opt/luttapy/
	pip3 install -r /opt/luttapy/requirements.txt
	cp ./luttapy.service /etc/systemd/system/luttapy.service
	cp -i ./luttapy.config.example /etc/luttapy.config
	systemctl daemon-reload
	@echo "After fixing /etc/luttapy.config file"
	@echo "Do systemctl enable luttapy.service"
	@echo "And systemctl start luttapy.service"
